#!/bin/bash

if [[ -z $1 ]] ; then
        echo "Syntax:

        $0 <root/of/git/repository>
"
        exit 1
fi

PULL=post-merge
PUSH=pre-push

cd ${1}/.git/hooks
if [[ -f $PULL ]]; then
        cp $PULL ${PULL}.backup
fi
if [[ -f $PUSH ]]; then
        cp $PUSH ${PUSH}.backup
fi
cd $OLDPWD
        
cp $PUSH $PULL pull.mp3  push.mp3  ${1}/.git/hooks
chmod +x ${1}/.git/hooks/$PUSH
chmod +x ${1}/.git/hooks/$PULL

if [[ $? == 0 ]] ; then
        echo "Installed successfully"
        exit 0
else
        echo "Problem in installation"
        exit -1
fi

